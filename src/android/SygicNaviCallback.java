package com.stanleyidesis.cordova.plugin;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.example.ezwaste.ezwastev3.Sygic3DActivity;
import com.example.ezwaste.ezwastev3.TripManageNavActivity;
import com.example.ezwaste.ezwastev3.enums.InstallationPointCollected;
import com.example.ezwaste.ezwastev3.management.DeviceManager;
import com.example.ezwaste.ezwastev3.struct.TwWaypoint;
import com.sygic.aura.embedded.IApiCallback;
import com.sygic.sdk.api.ApiDialog;
import com.sygic.sdk.api.ApiInfo;
import com.sygic.sdk.api.ApiItinerary;
import com.sygic.sdk.api.ApiMaps;
import com.sygic.sdk.api.ApiMenu;
import com.sygic.sdk.api.ApiNavigation;
import com.sygic.sdk.api.ApiOptions;
import com.sygic.sdk.api.ApiTrip;
import com.sygic.sdk.api.events.ApiEvents;
import com.sygic.sdk.api.exception.GeneralException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SygicNaviCallback implements IApiCallback {

    private Activity mActivity;
    public int tmp_waypoints = 2;
    Thread sygicThread = new Thread();

    public SygicNaviCallback(FragmentActivity activity) {
        mActivity = activity;
    }

    @Override
    public void onEvent(final int event, final String data) {
        switch (event) {
                    /*case ApiEvents.EVENT_WAYPOINT_VISITED: //
                        Log.d(LOG_TAG, "EVENT_WAYPOINT_VISITED");
                        Event evt = new Event("2259", "WAYPOINT", "");
                        sendJSON(evt.getJSONEvent(), Sync.EVENTS);
                        return;
                    case ApiEvents.EVENT_ROUTE_FINISH:

                        if(navigationType.equals("task")){
                            routeFinish = true;
                        }
                        show = false;
                        Log.d(LOG_TAG, "EVENT_RouteFinished");
                        Event evt2 = new Event("2201", "RouteFinished", "");
                        sendJSON(evt2.getJSONEvent(), Sync.EVENTS);
                        break;
                    /**
                     * when EXIT is received, we close the app
*/
            case ApiEvents.EVENT_APP_EXIT:
                //MainActivity.sRunning = false;

                sygicThread.interrupt();
                mActivity.finish();

                return;

            case ApiEvents.EVENT_WAYPOINT_CLICKED:
                Log.d("Sygic3D", "App EVENT_WAYPOINT_CLICKED");

                return;


            case ApiEvents.EVENT_APP_STARTED:

                Log.d("Sygic3D", "App started");
               sygicThread =  new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String itname = "itin-563";
                        ArrayList<TwWaypoint> lst = new ArrayList<>();
                        //Sygic3DActivity.installationPointList = TripManageNavActivity.installationPointList;

                        if(TripManageNavActivity.installationPointList.size() == 1){
                            tmp_waypoints = 1;  
                        }else if(TripManageNavActivity.installationPointList.size() < 1 ){
                            tmp_waypoints = 0;
                        }

                        int add = 0;

                        try {

                            boolean _TwoStepNav = DeviceManager.device.getTowStepNav();

                        //Starter Marker
                            if (_TwoStepNav == true) {
                                for (int j = 0; j < tmp_waypoints; j++) {
                                    if ((tmp_waypoints <= TripManageNavActivity.installationPointList.size())){
                                        if ((TripManageNavActivity.installationPointList.get(j).getCollected() != InstallationPointCollected.COLLECTED) && TripManageNavActivity.installationPointList.get(j).getPassedIP() == 0 ) {
                                            TwWaypoint c = new TwWaypoint(TripManageNavActivity.installationPointList.get(j).Latitude(), TripManageNavActivity.installationPointList.get(j).Longitude(), "via", TripManageNavActivity.installationPointList.get(j).getpID(), TripManageNavActivity.installationPointList.get(j).getpID());
                                            lst.add(c);
                                            add += 1;
                                        } else {
                                            tmp_waypoints += 1;
                                            add += 1;
                                        }
                                    }
                                }
                                tmp_waypoints += add;
                            }else{
                                for (int j = 0; j < TripManageNavActivity.installationPointList.size(); j++) {
                                    if((TripManageNavActivity.installationPointList.get(j).getCollected() != InstallationPointCollected.COLLECTED)){

                                        TwWaypoint c = new TwWaypoint(TripManageNavActivity.installationPointList.get(j).Latitude(), TripManageNavActivity.installationPointList.get(j).Longitude(), "via", TripManageNavActivity.installationPointList.get(j).getpID(), TripManageNavActivity.installationPointList.get(j).getpID());
                                        lst.add(c);
                                    }

                                }
                            }

                            String json = CreateJsonItinerary(lst);
                            // String itname = "itin-63";
                            ApiItinerary.addItinerary(json, itname, 0);
                            ApiItinerary.setRoute(itname, 0, 0);
                            if (DeviceManager.device.getAutoRouteOptimizer() == 0){
                                ApiItinerary.optimizeRoute(0);
                            }

                            //End Marker

                        } catch (GeneralException e) {
                         Log.e("Sygic3d", "Error: " + e.getCode() + ", " + e.getMessage());

                        e.printStackTrace();

                     }

                    }
                });


                sygicThread.start();

            case ApiEvents.EVENT_MAIN_MENU:
                break;

            case ApiEvents.EVENT_WAIPOINT_VISITED:
                try {

                    ApiDialog.flashMessage("Chegou ao seu destino",1000);
                } catch (GeneralException e) {
                    e.printStackTrace();
                }

                Log.d("Sygic3D", "App started");
                /*sygicThread =  new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String itname = "itin-563";
                        ArrayList<TwWaypoint> lst = new ArrayList<>();
                        //Sygic3DActivity.installationPointList = TripManageNavActivity.installationPointList;

                        if(TripManageNavActivity.installationPointList.size() == 1){
                            tmp_waypoints = 1;
                        }else if(TripManageNavActivity.installationPointList.size() < 1 ){
                            tmp_waypoints = 0;
                        }

                        int add = 0;

                        try {

                            boolean _TwoStepNav = DeviceManager.device.getTowStepNav();

                            //Starter Marker
                            if (_TwoStepNav == true) {
                                for (int j = 0; j < tmp_waypoints; j++) {
                                    if ((tmp_waypoints <= TripManageNavActivity.installationPointList.size())){
                                        if ((TripManageNavActivity.installationPointList.get(j).getCollected() != InstallationPointCollected.COLLECTED) && TripManageNavActivity.installationPointList.get(j).getPassedIP() == 0 ) {
                                            TwWaypoint c = new TwWaypoint(TripManageNavActivity.installationPointList.get(j).Latitude(), TripManageNavActivity.installationPointList.get(j).Longitude(), "via", TripManageNavActivity.installationPointList.get(j).getpID(), TripManageNavActivity.installationPointList.get(j).getpID());
                                            lst.add(c);
                                            add += 1;
                                        } else {
                                            tmp_waypoints += 1;
                                            add += 1;
                                        }
                                    }
                                }
                                tmp_waypoints += add;
                            }else{
                                *//*for (int j = 0; j < TripManageNavActivity.installationPointList.size(); j++) {
                                    if((TripManageNavActivity.installationPointList.get(j).getCollected() != InstallationPointCollected.COLLECTED)){

                                        TwWaypoint c = new TwWaypoint(TripManageNavActivity.installationPointList.get(j).Latitude(), TripManageNavActivity.installationPointList.get(j).Longitude(), "via", TripManageNavActivity.installationPointList.get(j).getpID(), TripManageNavActivity.installationPointList.get(j).getpID());
                                        lst.add(c);
                                    }

                                }*//*
                            }

                            String json = CreateJsonItinerary(lst);
                            // String itname = "itin-63";
                            ApiItinerary.addItinerary(json, itname, 0);
                            ApiItinerary.setRoute(itname, 0, 0);
                            if (DeviceManager.device.getAutoRouteOptimizer() == 0){
                                ApiItinerary.optimizeRoute(0);
                            }

                            //End Marker

                        } catch (GeneralException e) {
                            Log.e("Sygic3d", "Error: " + e.getCode() + ", " + e.getMessage());

                            e.printStackTrace();

                        }





                    }
                });


                sygicThread.start();*/


        }

    }


    public JSONObject createWaypoint(double lat, double lon, String time1, String time2, int waypointId, String wtype) {
        int ilat = (int)(lat * 100000);
        int ilon = (int)(lon * 100000);

        JSONObject obj = new JSONObject();
        try {
            obj.put("lat", Integer.valueOf(ilat));
            obj.put("lon", Integer.valueOf(ilon));
            obj.put("waypointId", Integer.valueOf(waypointId));
            obj.put("hitRadius", Integer.valueOf(200));
            obj.put("icon", "current_location");
            obj.put("type", wtype);

            if (time1 != null && time2 != null) {
                JSONObject tw = new JSONObject();
                tw.put("startTime", time1);
                tw.put("endTime", time2);
                tw.put("stopDelay", Integer.valueOf(180));

                tw.put("icon", "current_location");
                obj.put("timeWindow", tw);
            }
        }

        catch (JSONException e) {
            obj = null;
        }

        return obj;
    }


    public JSONObject createRoutepart(JSONObject wpFrom, JSONObject wpTo) {
        JSONObject obj = new JSONObject();

        try {
            if (wpFrom != null) obj.put("waypointFrom", wpFrom);
            obj.put("waypointTo", wpTo);
        }

        catch (JSONException e) {
            obj = null;
        }

        return obj;
    }


    public String CreateJsonItinerary(ArrayList<TwWaypoint> lst)  {
        try {
            JSONArray routeparts = new JSONArray();
            JSONObject routepart;
            JSONObject wpFrom;
            JSONObject wpTo;

            for (int i = 1; i < lst.size(); i++) {
                wpFrom = null;
                // WayPoint wp  = new WayPoint(233.4, 23.53, "s")
                if (i == 1) wpFrom = createWaypoint(lst.get(0).lat, lst.get(0).lon, lst.get(0).dtStart, lst.get(0).dtEnd, 0, lst.get(0).wpType);
                wpTo = createWaypoint(lst.get(i).lat, lst.get(i).lon, lst.get(i).dtStart, lst.get(i).dtEnd, i, lst.get(i).wpType);
                routepart = createRoutepart(wpFrom, wpTo);
                if (routepart == null) return null;
                routeparts.put(routepart);
            }

            JSONObject json = new JSONObject();
            json.put("name", "Bratislava-pickup");
            json.put("version", "2.2");
            json.put("routeParts", routeparts);

            return json.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }



    @Override
    public void onServiceConnected() {
    }

    @Override
    public void onServiceDisconnected() {

    }


}
