package com.stanleyidesis.cordova.plugin;
// The native Toast API

import android.widget.Toast;
// Cordova-required packages
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.graphics.BitmapFactory;
import org.apache.cordova.LOG;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import java.io.ByteArrayOutputStream;
import android.util.Base64;

public class ToastyPlugin extends CordovaPlugin {
    private static final String DURATION_LONG = "long";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if ("show".equals(action)) {

            Bitmap bmp = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawColor(Color.rgb(250, 0, 0));

            Paint paint = new Paint();

            paint.setColor(Color.BLACK);
            paint.setTextSize(40);
            canvas.drawText("Teste", 30, 100, paint);

            String message;
            String duration;
            PluginResult.Status status = PluginResult.Status.OK;
            String result = BitMapToString(bmp);

            try {
                JSONObject options = args.getJSONObject(0);
                message = options.getString("message") + "Alguma coisa";
                duration = options.getString("duration");
            } catch (JSONException e) {
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            }

            // Create the toast
            Toast toast = Toast.makeText(cordova.getActivity(), message ,
                    DURATION_LONG.equals(duration) ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
            // Display toast
            toast.show();
            // Send a positive result to the callbackContext
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
            callbackContext.sendPluginResult(pluginResult);
            return true;
        }

        return false;
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

}